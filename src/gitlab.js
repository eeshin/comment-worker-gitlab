import { gatherResponse } from './util';

class GitLab {
  constructor(projectID, privateKey) {
    // Constructor
	this.projectID = projectID;
    this.headers = {
		"PRIVATE-TOKEN": privateKey
	};
  }
  
    async getFileFromRepository(filePath, branchFrom = 'main') {
        const fileResponse = await fetch(                       
            `https://gitlab.com/api/v4/projects/${this.projectID}/repository/files/${filePath}?ref=${branchFrom}`,
        {
            method: 'GET',
            headers: this.headers
        }
        );
        const file = await gatherResponse(fileResponse);
        return file;
	}
	
	
	async createFileOnRepository(commentPath, message, content, branch) {
        const createCommentResponse = await fetch(
        `https://gitlab.com/api/v4/projects/${this.projectID}/repository/files/${encodeURIComponent(commentPath)}`,
            {
                method: 'POST',
                body: JSON.stringify({
                    "branch" : branch,
                    "commit_message" : message,
                    "content" : content
                }),
                headers : {
                        ...this.headers,
                        "Content-Type": "application/json"	
                }
            }
        );
	    const branchCreation = await gatherResponse(createCommentResponse);
        return branchCreation;
	}
	
    async createBranchOnRepository(branchName, branchFrom = 'main') {
        const createBranchResponse = await fetch(
            `https://gitlab.com/api/v4/projects/${this.projectID}/repository/branches`,
            {
                method: 'POST',
                body: JSON.stringify({
                "branch": branchName,
                "ref": branchFrom
                }),
                headers: {
                    ...this.headers,
                    "Content-Type": "application/json"
                }
            }
        );
        const branchCreation = await gatherResponse(createBranchResponse);
        return branchCreation;
    }
	
	
	
	async createPullRequestOnRepository(title, body, branchSource, branchTarget = 'main') {
        const createPullRequestResponse = await fetch(
            `https://gitlab.com/api/v4/projects/${this.projectID}/merge_requests`,
            {
                method: 'POST',
                body: JSON.stringify({
                    "source_branch" : branchSource,
                    "target_branch" : branchTarget,
                    "title" : title,
                    "description" : body
                }),
                headers: {
                    ...this.headers,
                    "Content-Type": "application/json"
                }
            }
        );
        const pullRequest = await gatherResponse(createPullRequestResponse);
        return pullRequest;
    }
	
	
}


export default GitLab;