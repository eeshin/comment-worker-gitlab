
This is a modified version of comment-worker v0.0.3. This supports Gitlab only.

Original source came from https://github.com/zanechua/comment-worker.

# Usage

Add `staticman.yml` to the root of your repository. [Example on Github](https://github.com/zanechua/website/blob/master/staticman.yml)

We're using Cloudflare workers + wrangler to handle the deployments, we have to use wrangler due to the features we are using that requires wrangler to package the bundle up.

You need to declare the following variables in the Cloudflare worker variable settings.

You can use the `example config` section in the `wrangler.toml`, just uncomment the section and replace with your own values.

| key                      | example                                     | description                                                           |
|--------------------------|---------------------------------------------|-----------------------------------------------------------------------|
| GITLAB_PROJECT_ID        | 12345678                                    | A Project ID of the GitLab project to commit                          |
| GITLAB_REPOSITORY_BRANCH | main                                        | Branch where the site configuration lies                              |
| GITLAB_PRIVATE_TOKEN     | glpat-eSrgAWQRpiow                          | A Gitlab Personal Access Token to access the repository               |
| CW_ALLOWED_ORIGINS       | https://example.com,https://www.example.com | Allowed origins that can send a request to the comment-worker         |
| CW_DEBUG                 | false                                       | Turning debug mode on or off                                          |

GITLAB_PRIVATE_TOKEN can be added using Secrets store.

```shell
>pnpm install
>pnpm wrangler secret put GITLAB_PRIVATE_TOKEN
 ⛅️ wrangler 3.7.0 (update available 3.32.0)
------------------------------------------------------
√ Enter a secret value: ... **************************
🌀 Creating the secret for the Worker "comment-worker-gitlab"
✨ Success! Uploaded secret GITLAB_PRIVATE_TOKEN
>
```

You need to run the deploy command with the `CLOUDFLARE_ACCOUNT_ID` and `CLOUDFLARE_API_TOKEN` variables before running the command.

```bash
CLOUDFLARE_ACCOUNT_ID=abc CLOUDFLARE_API_TOKEN=abc pnpm wrangler deploy
```


# Development Setup

Follow the setup section above first.

Create a `.dev.vars` at the root of the repository and the format should follow the `dotenv` format of `KEY=VALUE` and a new line for each of the environment variables

Run the following to load up the app
```bash
pnpm install
pnpm dev
```
